<?php

namespace App\Http\Controllers\UserControllers;

use App\Http\Traits\ProMatTrait;
use App\Models\Cart;
use App\Models\Product;
use App\Models\ProductRatings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\ProductReviews;
use Illuminate\Support\Facades\Validator;

use function GuzzleHttp\Promise\all;

class CartController extends Controller
{
    use ProMatTrait;

    public function cart_item()
    {
        try {
            $cartItems =   DB::table('carts')->count();
            return  $cartItems;
        } catch (\Throwable $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }
    public function cart_total()
    {
        try {
            $cart_total = DB::table('carts')->where('user_id', Auth::user()->id)->sum('total');
            $shipping_price = $_REQUEST['id'] +  $cart_total;
            $cart_prodcts = Cart::all();
            return  ['cart_total' => $cart_total, 'price' => $shipping_price, 'cart_products' => $cart_prodcts];
        } catch (\Throwable $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }


    public function addToCart(Request $request)
    {
        try {
            $product = Product::find($request->id);
            $product_price = $product->price;

            $cart_product =  DB::table('carts')
                ->where('product_id', $product->id)
                ->first();

            if ($cart_product != null) {
                Cart::where('name', $product->name)->update([
                    'total' => $product_price * $request->quantity,
                    'quantity' => $request->quantity,

                ]);
                session()->flash('success', 'Product updated Successfully into Cart!');
            } else {

                $validator = Validator::make($request->all(), [
                    'size' => 'required',
                    'color' => 'required',

                ]);
                if ($validator->fails()) {
                    session()->flash('error', 'Please select size and color');
                    return redirect()->back();


                } else {
                    Cart::create([
                        'id' => $request->id,
                        'size' => $request->size,
                        'color' => $request->color,
                        'name' => $request->name,
                        'price' => $request->price,
                        'quantity' => $request->quantity,
                        'product_id' =>  $product->id,
                        'user_id' =>  Auth::user()->id,
                        'total' => ($product_price * $request->quantity),

                    ]);
                    session()->flash('success', 'Product Added Successfully into Cart!');
                }
            }

            return redirect()->route('products.list');
        } catch (\Throwable $e) {
            // dd($e);
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function updateCart(Request $request)
    {
        //   dd($request->all());
        try {
            $item = Cart::find($request->id);

            $item_price = Product::where('id', $item->product_id)->first();
            Cart::find($request->id)->update([
                'quantity' => $request->quantity,
                'total' => $request->quantity * $item_price->price,
            ]);

            session()->flash('success', 'Item Cart is Updated Successfully !');

            return redirect()->route('cart.list');
        } catch (\Throwable $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function removeCart(Request $request)
    {
        try {
            Cart::destroy($request->id);
            session()->flash('success', 'Item Cart Remove Successfully !');

            return redirect()->route('cart.list');
        } catch (\Throwable $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function clearAllCart(Request $request)
    {
        try {

            DB::delete('delete from carts');
            session()->flash('success', 'All Item Cart Clear Successfully !');

            return redirect()->route('cart.list');
        } catch (\Throwable $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function product_starrating(Request $request)
    {
        // dd($request->all());
        try {

            $data= new ProductRatings();

            if($request->file('image')){
                $file= $request->file('image');
                $filename= date('YmdHi').$file->getClientOriginalName();
                $file-> move(public_path('public/Image'), $filename);
                $data['image']= $filename;
            }
            $data->name = $request->name;
            $data->review = $request->review;
            $data->name = $request->name;
            $data->product_id = $request->id;
            $data->rating = $request->rating;
            $data->save();

            session()->flash('success', 'Review Sent Successfully');

            return redirect()->back();
        } catch (\Throwable $e) {
            // dd($e);
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }


    public function search()
    {
        try {
            $search = $_GET['query'];
            $products = Product::where('name', 'LIKE', '%' . $search . '%')->get();
            // dd($products);

            return view('User/shop', compact('products'));
        } catch (\Throwable $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
