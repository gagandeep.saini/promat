<?php

namespace App\Http\Controllers\UserControllers;

use App\Http\Traits\ProMatTrait;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class HomeController extends Controller
{
    use ProMatTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function profileUpdate(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'email' => 'required|email|unique:users,email,' . Auth::user()->id,
                'mobile_number' => 'required|integer',
                'state' => 'required|string',
                'country' => 'required|string',
                'postcode' => 'required|integer',
                'address' => 'required|string'
            ]);
            if ($validator->fails()) {
                return redirect('profile')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $user_query = DB::table('users')
                    ->select('token')
                    ->where('id', '=', Auth::user()->id)
                    ->first();
                if ($user_query != null) {
                    $query = User::find(Auth::user()->id)->update([
                        'name' => $request->input('name', null),
                        'email' => $request->email,
                        'mobile_number' => $request->mobile_number,
                        'address' => $request->address,
                        'postcode' => $request->postcode,
                        'state' => $request->state,
                        'country' => $request->country,

                    ]);
                    if ($query != null) {
                        return redirect()->back()->with('success', 'User Profile successfully updated');
                    } else {
                        return redirect()->back()->with('error', 'Failed to Update');
                    }
                } else {
                    return redirect()->back()->with('error', 'user not found');
                }
            }
        } catch (\Throwable $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }
    public function uploadPicture(Request $request)
    {
        try {
            if ($request->hasFile('image')) {
                $filename = $request->image->getClientOriginalName();
                $request->image->storeAs('images', $filename, 'public');
                // Auth()->user()->update(['image' => $filename]);
                DB::table('users')
                    ->where('id', Auth()->user()->id)
                    ->update([
                        'image' => $filename
                    ]);
            }
            return redirect()->back();
        } catch (\Throwable $e) {
            // dd($e);
            return response()->json([

                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    function changePassword(Request $request)
    {
        try {
            //Validate form
            $validator = Validator::make($request->all(), [
                'password' => [
                    'required', function ($attribute, $value, $fail) {
                        if (!Hash::check($value, Auth::user()->password)) {
                            return $fail(__('The current password is incorrect'));
                        }
                    },
                    'min:8',
                    'max:30'
                ],
                'new_password' => 'required|min:8|max:30',
                'confirm_password' => 'required|same:new_password'
            ], [
                'password.required' => 'Enter your current password',
                'password.min' => 'Old password must have atleast 8 characters',
                'password.max' => 'Old password must not be greater than 30 characters',
                'new_password.required' => 'Enter new password',
                'new_password.min' => 'New password must have atleast 8 characters',
                'new_password.max' => 'New password must not be greater than 30 characters',
                'confirm_password.required' => 'ReEnter your new password',
                'confirm_password.same' => 'New password and Confirm new password must match'
            ]);

            if ($validator->fails()) {
                return redirect('profile')
                    ->withErrors($validator)
                    ->withInput();
            } else {

                $update = User::find(Auth::user()->id)->update(['password' => Hash::make($request->new_password)]);

                if (!$update) {
                    // return response()->json(['status' => 0, 'msg' => 'Something went wrong, Failed to update password in db']);
                    return redirect()->back()->with('error', 'Something went wrong, Failed to update password in db');
                } else {
                    return redirect()->back()->with('success', 'You are now successfully registerd');
                }
            }
        } catch (\Throwable $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }


    function delete_user($id)
    {
        try {
            User::destroy($id);

            return redirect('all_users');
        } catch (\Throwable $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function edit_user(Request $request)
    {
        // dd($request->all());
        try {
            $query = User::where('id', $request->id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'mobile_number' => $request->mobile_number,
                'address' => $request->address,
                'postcode' => $request->postcode,
                'state' => $request->state,
                'country' => $request->country,

            ]);

            if (!$query) {
                return response()->json(['status' => 0, 'msg' => 'Something went wrong.']);
            } else {
                return redirect('all_users');
            }
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function users_queries(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'email' => 'required|email',
                'subject' => 'required|string',
                'message' => 'required|string',
            ]);
            if ($validator->fails()) {
                return redirect()->back()->with('error', 'Query Send Successfully')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                DB::table('user_queries')
                    ->insert([
                        'name' => $request->input('name', null),
                        'email' => $request->email,
                        'subject' => $request->subject,
                        'message' => $request->message,
                    ]);
                return redirect()->back()->with('success', 'Query Send Successfully');
            }
        } catch (\Throwable $e) {
            // dd($e);
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function user_addresses(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'street' => 'required|string',
                'landmark' => 'required|string',
                'email' => 'required|email',
                'mobile_number' => 'required|integer',
                'state' => 'required|string',
                'country' => 'required|string',
                'postcode' => 'required|integer',
                'address' => 'required|string'
            ]);
            if ($validator->fails()) {
                return redirect('checkout')
                    ->withErrors($validator)
                    ->withInput();
            } else {

                UserAddress::create([
                    'id' => $request->id,
                    'name' => $request->name,
                    'user_id' => Auth::user()->id,
                    'mobile_number' => $request->mobile_number,
                    'postcode' => $request->postcode,
                    'email' => $request->email,
                    'address' => $request->address,
                    'street' => $request->street,
                    'landmark' => $request->landmark,
                    'state' => $request->state,
                    'country' => $request->country,

                ]);
                session()->flash('success', 'Address Added Successfully !');

                return redirect()->route('checkout')->withInput();
            }
        } catch (\Throwable $e) {
            // dd($e);
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }



    public function order_placed(Request $request)
    {
        try {
            $last = DB::table('user_addresses')->latest()->first();
            $cart = DB::table('carts')->where('user_id', Auth::user()->id)->first();
            Order::create([
                        'id' => $last->id,
                        'name' => $last->name,
                        'mobile_number' => $last->mobile_number,
                        'street' => $last->street,
                        'landmark' => $last->landmark,
                        'address' => $last->address,
                        'postcode' => $last->postcode,
                        'state' => $last->state,
                        'email' => $last->email,
                        'country' => $last->country,
                        'total' => $cart->total,

                    ]);


        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }


}
