<?php

namespace App\Http\Controllers\UserControllers;

use App\Http\Controllers\UserControllers\Controller;
use Exception;
use Razorpay\Api\Api;

// use Facade\FlareClient\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class RazorpayPaymentController extends Controller
{
    public function index()
    {
        return view('razorpayView');
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));

        $payment = $api->payment->fetch($input['razorpay_payment_id']);

        if(count($input)  && !empty($input['razorpay_payment_id'])) {
            try {
                $response = $api->payment->fetch($input['razorpay_payment_id'])->capture(array('amount'=>$payment['amount']));

            } catch (Exception $e) {
                return  $e->getMessage();
                Session::put('error',$e->getMessage());
                return redirect()->back();
            }
        }
        session()->flash('success', 'Payment Successfully !');

        return redirect()->route('order.placed');
    }
}
