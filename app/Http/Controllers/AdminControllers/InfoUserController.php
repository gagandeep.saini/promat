<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\AdminControllers\Controller;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use App\Models\UserQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\View;

class InfoUserController extends Controller
{

    public function create()
    {
        return view('Admin/laravel-examples/user-profile');
    }

    public function store(Request $request)
    {

        $attributes = request()->validate([
            'name' => ['required', 'max:50'],
            'email' => ['required', 'email', 'max:50', Rule::unique('users')->ignore(Auth::user()->id)],
            'mobile_number' => ['max:50'],
            'address' => ['max:70'],
            'country'    => ['max:150'],
            'postcode'    => ['required'],
            'image'    => ['required'],
        ]);
        if ($request->get('email') != Auth::user()->email) {
            if (env('IS_DEMO') && Auth::user()->id == 1) {
                return redirect()->back()->withErrors(['msg2' => 'You are in a demo version, you can\'t change the email address.']);
            }
        } else {
            $attribute = request()->validate([
                'email' => ['required', 'email', 'max:50', Rule::unique('users')->ignore(Auth::user()->id)],
            ]);
        }


        if (!empty($_FILES['image']['name'])) {
            $image_path = public_path()."/img/".$attributes['image'];
           $newFileName = rand(111, 999) . time() . '_' . $_FILES['image']['name'];
           move_uploaded_file($_FILES['image']['tmp_name'], public_path().'/img/'.$newFileName);
          $attributes['image'] =  $newFileName;
      }

        User::where('id', Auth::user()->id)
            ->update([
                'name' => $attributes['name'],
                'email' => $attribute['email'],
                'mobile_number' => $attributes['mobile_number'],
                'address' => $attributes['address'],
                'country'    => $attributes["country"],
                'postcode'    => $attributes["postcode"],
                'image'    => $attributes["image"],
            ]);


        return redirect('/user-profile')->with('success', 'Profile updated successfully');
    }

    public function all_users(Request $request)
    {
        try {
            $user = User::get()->toArray();
            return view('Admin/laravel-examples/user-management')->with(compact('user'));
        } catch (\Throwable $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function delete($id)
    {
        try {
            User::destroy($id);
            return redirect('/user-management');
        } catch (\Throwable $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }
    public function edit(Request $request)
    {


        try {
            $user = User::where('id', $request->id)->first()->toArray();
            return view('Admin/laravel-examples/user-management-edit')->with(compact('user'));
        } catch (\Throwable $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }
    public function edit_user(Request $request)
    {
        // dd($request->all());
        try {
            $attributes = request()->validate([
                'name' => ['required', 'max:50'],
                'email' => ['required', 'email', 'max:50'],
                'mobile_number' => ['max:50'],
                'address' => ['max:70'],
                'country'    => ['max:150'],
                'postcode'    => ['required'],
                'image'    => ['required'],
            ]);

            if (!empty($_FILES['image']['name'])) {
                $image_path = public_path()."/img/".$attributes['image'];
               $newFileName = rand(111, 999) . time() . '_' . $_FILES['image']['name'];
               move_uploaded_file($_FILES['image']['tmp_name'], public_path().'/img/'.$newFileName);
              $attributes['image'] =  $newFileName;
          }

            User::where('id', $request->id)
                ->update([
                    'name' => $attributes['name'],
                    'email' => $attributes['email'],
                    'mobile_number' => $attributes['mobile_number'],
                    'address' => $attributes['address'],
                    'country'    => $attributes["country"],
                    'postcode'    => $attributes["postcode"],
                    'image'    => $attributes["image"],
                ]);


            return redirect('user-management');
        } catch (\Throwable $e) {
            // dd($e);
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function dashboard(Request $request)
    {
        try {
            $user = User::count();
            $product = Product::count();
            $order = Order::count();
            $queries = UserQuery::count();
            return view('Admin/dashboard')->with(compact('user', 'product', 'order', 'queries'));
        } catch (\Throwable $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }
    public function orders(Request $request)
    {
        try {

            $order = Order::get()->toArray();
            return view('Admin/orders')->with(compact('order'));
        } catch (\Throwable $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }
    public function products(Request $request)
    {
        try {
            $pro = Product::paginate(10);

            return view('Admin/products')->with(compact('pro'));
        } catch (\Throwable $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function products_delete($id)
    {
        try {
            Product::destroy($id);
            return redirect('/products');
        } catch (\Throwable $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function products_edit(Request $request)
    {
        try {

            $product = Product::where('id', $request->id)->first()->toArray();
            return view('Admin/products-edit')->with(compact('product'));
        } catch (\Throwable $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }
    public function edit_products(Request $request)
    {
        // dd($product);
        try {

            $restaurant =  Product::where('id',$request->id)->first();
            $restaurant->name =  $request->input('name');
            $restaurant->price =  $request->input('price');
            if (!empty($_FILES['image']['name'])) {
                //  dd($_FILES['image']['name']);
                $image_path = public_path()."/img/".$restaurant->image;
                //  if(File::exists($image_path)) {
                //      File::delete($image_path);
                //  }
               $newFileName = rand(111, 999) . time() . '_' . $_FILES['image']['name'];
               move_uploaded_file($_FILES['image']['tmp_name'], public_path().'/img/'.$newFileName);
              $restaurant->image =  $newFileName;
          }

             $restaurant->save();
            return redirect('products');
        } catch (\Throwable $e) {
            // dd($e);
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function new_product()
    {
        try {
            return view('Admin/new-product');
        } catch (\Throwable $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function add_product(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'price' => ['required', 'integer'],
                'description' => ['required', 'string'],
                'image' => ['required'],

            ]);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator)
                ->withInput();
            } else {

                $data = new Product();
                $data->name = $request->name;
                $data->price = $request->price;
                $data->description = $request->description;


                if (!empty($_FILES['image']['name'])) {
                    $image_path = public_path()."/img/".$data->image;
                   $newFileName = rand(111, 999) . time() . '_' . $_FILES['image']['name'];
                   move_uploaded_file($_FILES['image']['tmp_name'], public_path().'/img/'.$newFileName);
                  $data->image =  $newFileName;
              }

                $data->save();

                return redirect('products');
            }
        } catch (\Throwable $e) {
            // dd($e);
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function search_Product()
    {
        try {
            $search = $_GET['query'];
            $pro= Product::where('name', 'LIKE', '%' . $search . '%')->get();
            // dd($products);

            return view('Admin/products', compact('pro'));
        } catch (\Throwable $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
