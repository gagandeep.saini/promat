<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\AdminControllers\Controller;
use Illuminate\Http\Request;

class AdminHomeController extends Controller
{
    public function home()
    {
        return redirect('dashboard');
    }
}
