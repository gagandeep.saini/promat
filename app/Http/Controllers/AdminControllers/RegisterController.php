<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\AdminControllers\Controller;
use App\Models\User;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    public function create()
    {
        return view('Admin/session.register');
    }

    public function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'postcode' => ['required'],
            'mobile_number' => ['required'],
            'country' => ['required', 'string'],
            'state' => ['required', 'string'],
            'address' => ['required', 'string'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);
            if ($validator->fails()) {
                return redirect()->back()
                ->withErrors($validator)
                ->withInput();
            } else {

                $data = new User();
                $data->name = $request->name;
                $data->email = $request->email;
                $data->postcode = $request->postcode;
                $data->mobile_number = $request->mobile_number;
                $data->country = $request->country;
                $data->state = $request->state;
                $data->address = $request->address;
                $data->role = $request->role;
                $data->token = $request->_token;
                $data->password = bcrypt($request->password);
                $data->save();
                Auth::login($data);

                return redirect('/dashboard');
            }
        } catch (\Throwable $e) {
            // dd($e);
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
