<?php
namespace App\Http\Traits;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use App\Models\UserQuery;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

trait ProMatTrait {
    public function all_users()
    {
        $user = User::where('role', '=', 2)->get();
        return view('User/usersList')->with(compact('user'));
    }
    public function index()
    {
        $products = Product::paginate(8);

        return view('User/home', compact('products'));
    }
    public function profile()
    {
        return view('User/profile');
    }
    public function order_placed_view()
    {
        $last = DB::table('user_addresses')->latest()->first();
        $cart = DB::table('carts')->where('user_id', Auth::user()->id)->sum('total');
        $cart_total = $cart;
        Order::create([
                    'id' => $last->id,
                    'name' => $last->name,
                    'mobile_number' => $last->mobile_number,
                    'street' => $last->street,
                    'landmark' => $last->landmark,
                    'address' => $last->address,
                    'postcode' => $last->postcode,
                    'state' => $last->state,
                    'email' => $last->email,
                    'country' => $last->country,
                    'total' =>  $cart_total,

                ]);

                DB::table('carts')->where('user_id',  Auth::user()->id)->delete();


        return view('User/order_placed');
    }
    public function shop()
    {

        return view('User/shop');
    }
    public function cart()
    {
        return view('User/cart');
    }
    public function contact()
    {
        return view('User/contact');
    }
    public function checkout()
    {
        $total = Cart::where('user_id', Auth()->user()->id)->sum('total');
        $cart_total = $total * 100;
        return view('User/checkout')->with(compact('cart_total'));
    }
    public function shop_detail()
    {
        $id = base64_decode($_REQUEST['id']);
        $user_detail = Product::where('id', $id)->first()->toarray();
        $cart_value = Cart::where('product_id', $id)->select('quantity','size','color')->first();
        $products = Product::all();
        $count_review = DB::table('product_ratings')->where('product_id',$id)->count('id');
        $show_review = DB::table('product_ratings')->where('product_id',$id)->get()->toArray();
        // $count_reviews = $show_review->id;

        return view('User/shopDetail', compact('user_detail', 'cart_value' , 'count_review','show_review' ,'products'));
        }
    public function users_queries_view()
    {
        $user = UserQuery::all();
        return view('User/userquery')->with(compact('user'));
    }
    public function edit_user_view($id)
    {
        $user_id = $id;
        $query = User::where('id', $user_id)->first();
        return view('User/editUserDetail')->with(compact('user_id' , 'query'));
    }
    public function cartList()
    {
        $cartItems = Cart::get();
        return view('User/cart', compact('cartItems'));
    }
    public function productList()
    {

        $products = Product::get();

        return view('User/shop', compact('products'));
    }
}
?>
