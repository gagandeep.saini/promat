<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductRatings extends Model
{
    use HasFactory;
    public $table = 'product_ratings';

    protected $fillable = [
        'name',
        'review',
        'product_id',
        'rating',
        'image',
    ];
}
