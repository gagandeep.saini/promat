<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'user_id',
        'mobile_number',
        'postcode',
        'email',
        'address',
        'street',
        'landmark',
        'state',
        'country',
    ];

}
