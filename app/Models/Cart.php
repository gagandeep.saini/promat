<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    public $table = 'carts';

    protected $fillable = [
        'name',
        'price',
        'quantity',
        'size',
        'color',
        'product_id',
        'user_id',
        'total',
    ];

}
