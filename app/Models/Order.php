<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    public $table = 'orders';

    protected $fillable = [
        'name',
        'mobile_number',
        'street',
        'landmark',
        'address',
        'postcode',
        'state',
        'email',
        'country',
        // 'product_name',
        // 'product_price',
        'total',
    ];
}
