@extends('Admin.layouts.user_type.auth')

@section('content')
    <div>

        <div class="row">
            <div class="col-12">
                <div class="card mb-4 mx-4">
                    <div class="card-header pb-0">
                        <div class="d-flex flex-row justify-content-between">
                            <div>
                                <h5 class="mb-0">All Products</h5>
                            </div>
                                <form action="{{ route('product.search') }}" method="Get" enctype="multipart/form-data">
                                    @csrf
                                    <div class="input-group">
                                        <input type="text" name="query" class="form-control" placeholder="Search by name">
                                        <div class="input-group-append">
                                            <span class="input-group-text bg-transparent text-primary">
                                                <button class="" type="submit" style="border: none">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>


                                </form>
                            <a href="products/add" class="btn bg-gradient-primary btn-sm mb-0" type="button">+&nbsp; Add New Product</a>
                        </div>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            ID
                                        </th>
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            Product Name
                                        </th>
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Product Photo
                                        </th>
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Product Price
                                        </th>

                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Edit
                                        </th>
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Delete
                                        </th>


                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($pro as $products)
                                    {{-- {{dd(url('/'))}} --}}
                                        <tr>
                                            <td class="text-center">
                                                <p class="text-xs font-weight-bold mb-0">{{ $products['id'] }}</p>
                                            </td>

                                            <td class="text-center">
                                                <p class="text-xs font-weight-bold mb-0">{{ $products['name'] }}</p>
                                            </td>
                                            <td class="text-center">
                                                <div>
                                                    <img src="{{ '/img/'.$products['image'] }}" class="avatar avatar-sm me-3">
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <p class="text-xs font-weight-bold mb-0">{{ $products['price'] }}</p>
                                            </td>



                                            <td class="text-center">

                                                    <a  href="{{ route('products.edit', ['id'=> $products['id']]) }}" class="mx-3"
                                                    data-bs-toggle="tooltip" data-bs-original-title="Edit Product">
                                                        <i class="fas fa-user-edit text-secondary"></i>
                                                    </a>
                                                </td>
                                                <td class="text-center">
                                                <a href="products/delete/{{ $products['id'] }}" class="mx-3"
                                                    data-bs-toggle="tooltip" data-bs-original-title="Delete Product">
                                                    <i class="fas fa-trash text-secondary"></i>
                                                </a>
                                            </td>

                                        </tr>
                                    @endforeach

                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


