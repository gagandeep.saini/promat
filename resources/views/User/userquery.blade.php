@extends('User.layouts.master')


@section('content')
    <div class="container mt-5">
        <table class="table table-inverse">
            <thead>
                <tr>
                    <th>Users</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Message</th>
                    <th>Subject</th>

                </tr>
            </thead>
            <tbody>
                @foreach ($user as $data)
                    <tr id="student{{ $data->id }}">
                        <td>{{ $data->id }}</td>
                        <td>{{ $data->name }}</td>
                        <td>{{ $data->email }}</td>
                        <td>{{ $data->message }}</td>
                        <td>{{ $data->subject }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
