@extends('User.layouts.master')


@section('content')

    <div class="container-fluid">
        @if (Session::get('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        @endif
        @if (Session::get('error'))
            <div class="alert alert-danger">
                {{ Session::get('error') }}
            </div>
        @endif
        <div class="container rounded bg-white mt-5 mb-5">
            <div class="row">

                <div class="col border-right">
                    <form method="post" action="{{ url('all_users/edit/') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="p-3 py-5">
                            <div class="d-flex justify-content-between align-items-center mb-3">
                                <h4 class="text-right">Profile Settings</h4>
                            </div>
                            <input type="hidden" value="{{$user_id}}" name="id">

                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <label class="labels">Name</label>
                                    <input type="text" name="name"
                                        class="form-control @error('name') is-invalid @enderror"
                                        value="{{$query->name}}" >
                                    @error('name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-12"><label class="labels">Email ID</label><input type="text"
                                    name="email" class="form-control @error('email') is-invalid @enderror"
                                    value="{{$query->email}}">
                                    @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-12">
                                    <label class="labels">Mobile Number</label>
                                    <input type="text" name="mobile_number"
                                        class="form-control @error('mobile_number') is-invalid @enderror"
                                        value="{{$query->mobile_number}}">
                                    @error('mobile_number')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-12"><label class="labels">Address</label><input type="text"
                                    name="address" class="form-control @error('address') is-invalid @enderror"
                                        value="{{$query->address}}">
                                    @error('address')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-12"><label class="labels">Postcode</label><input type="text"
                                        name="postcode" class="form-control @error('postcode') is-invalid @enderror"
                                        value="{{$query->postcode}}">
                                    @error('postcode')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-12"><label class="labels">State</label><input type="text"
                                        name="state" class="form-control @error('state') is-invalid @enderror"
                                        value="{{$query->state}}">
                                    @error('state')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-12"><label class="labels">Country</label><input type="text"
                                        name="country" class="form-control @error('country') is-invalid @enderror"
                                        value="{{$query->country}}" >
                                    @error('country')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="mt-5 text-center">
                                <button class="btn btn-primary profile-button" type="submit">
                                    save
                                </button>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    </div>
    </div>
@endsection
