@extends('User.layouts.master')

@section('content')
    <!-- Topbar Start -->




    <!-- Shop Detail Start -->
    <div class="container-fluid py-5">

        @if (Session::get('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif
    @if (Session::get('error'))
        <div class="alert alert-danger">
            {{ Session::get('error') }}
        </div>
    @endif
        {{-- {{dd($cart_value['size']);}} --}}
        <div class="row px-xl-5">
            <div class="col-lg-5 pb-5">
                <div id="product-carousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner border">
                        <div class="carousel-item active">
                            <img class="w-100 h-100" src="{{ '/img/'.$user_detail['image'] }}" alt="Image">
                        </div>
                        <div class="carousel-item">
                            <img class="w-100 h-100" src="{{  '/img/'.$user_detail['image'] }}" alt="Image">
                        </div>
                        <div class="carousel-item">
                            <img class="w-100 h-100" src="{{  '/img/'.$user_detail['image'] }}" alt="Image">
                        </div>
                        <div class="carousel-item">
                            <img class="w-100 h-100" src="{{  '/img/'.$user_detail['image'] }}" alt="Image">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#product-carousel" data-slide="prev">
                        <i class="fa fa-2x fa-angle-left text-dark"></i>
                    </a>
                    <a class="carousel-control-next" href="#product-carousel" data-slide="next">
                        <i class="fa fa-2x fa-angle-right text-dark"></i>
                    </a>
                </div>
            </div>

            <div class="col-lg-7 pb-5">
                {{-- {{dd($user_detail);}} --}}
                <h3 class="font-weight-semi-bold">{{ $user_detail['name'] }}</h3>
                <div class="d-flex mb-3">

                    <small class="pt-1"> ({{ $count_review }} Reviews)</small>
                </div>
                <h3 class="font-weight-semi-bold mb-4">${{ $user_detail['price'] }}</h3>
                <p class="mb-4">{{ $user_detail['description'] }}</p>
                <form action="{{ route('cart.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="d-flex mb-3">

                        <p class="text-dark font-weight-medium mb-0 mr-3 ">Sizes:</p>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" value="xs" class="custom-control-input" id="size-1" name="size">
                            <label class="custom-control-label" for="size-1">XS</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" value="S" class="custom-control-input" id="size-2" name="size">
                            <label class="custom-control-label" for="size-2">S</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" value="M" class="custom-control-input" id="size-3" name="size">
                            <label class="custom-control-label" for="size-3">M</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" value="L" class="custom-control-input" id="size-4" name="size">
                            <label class="custom-control-label" for="size-4">L</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" value="xl" class="custom-control-input" id="size-5" name="size">
                            <label class="custom-control-label" for="size-5">XL</label>
                        </div>
                    </div>
                    <div class="d-flex mb-4">
                        <p class="text-dark font-weight-medium mb-0 mr-3">Colors:</p>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input value="black" type="radio" class="custom-control-input" id="color-1" name="color">
                            <label class="custom-control-label" for="color-1">Black</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input value="white" type="radio" class="custom-control-input" id="color-2"
                                name="color">
                            <label class="custom-control-label" for="color-2">White</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input value="red" type="radio" class="custom-control-input" id="color-3"
                                name="color">
                            <label class="custom-control-label" for="color-3">Red</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input value="blue" type="radio" class="custom-control-input" id="color-4"
                                name="color">
                            <label class="custom-control-label" for="color-4">Blue</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input value="green" type="radio" class="custom-control-input" id="color-5"
                                name="color">
                            <label class="custom-control-label" for="color-5">Green</label>

                        </div>
                    </div>
                    <div class="d-flex align-items-center mb-4 pt-2">
                        <div class="input-group quantity mr-3" style="width: 130px;">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-primary btn-minus">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <input name="quantity" class="form-control bg-secondary text-center"
                                value="{{ $cart_value['quantity'] ?? 1 }}">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-primary btn-plus">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <input type="hidden" value="{{ $user_detail['id'] }}" name="id">
                        <input type="hidden" value="{{ $user_detail['name'] }}" name="name">
                        <input type="hidden" value="{{ $user_detail['price'] }}" name="price">

                        <button type="submit" id="radiobutton" class="btn btn-primary px-3"><i
                                class="fa fa-shopping-cart mr-1"></i> Add
                            To Cart</button>
                    </div>
                </form>
                <div class="d-flex pt-2">
                    <p class="text-dark font-weight-medium mb-0 mr-2">Share on:</p>
                    <div class="d-inline-flex">
                        <a class="text-dark px-2" href="">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a class="text-dark px-2" href="">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a class="text-dark px-2" href="">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                        <a class="text-dark px-2" href="">
                            <i class="fab fa-pinterest"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row px-xl-5">
            <div class="col">
                <div class="nav nav-tabs justify-content-center border-secondary mb-4">
                    <a class="nav-item nav-link active" data-toggle="tab" href="#tab-pane-1">Reviews
                        ({{ $count_review }})</a>
                    <a class="nav-item nav-link" data-toggle="tab" href="#tab-pane-2">Information</a>
                    <a class="nav-item nav-link" data-toggle="tab" href="#tab-pane-3">Description</a>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="tab-pane-1">

                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="mb-4"> ({{ $count_review }}) reviews for {{ $user_detail['name'] }}</h4>
                                <div class="media mb-4">

                                    <div class="media-body">
                                        @foreach ($show_review as $show_reviews)
                                        <div class="d-flex">
                                           <div>
                                                <img src="{{ url('public/Image/'.$show_reviews->image) }}"
                                                style="height: 100px; width: 100px;">
                                                <br>
                                                <br>
                                            </div>
                                                <div class="justify-content-center text-left" style="margin-left: 10px">
                                            <h6>User Name : {{ $show_reviews->name }}</h6>
                                            <h6 >Product Rating : {{ $show_reviews->rating }} *
                                                @if ($show_reviews->rating == 1)


                                                <div class="text-primary mr-2">
                                                    <small class="fas fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                </div>
                                                @elseif ($show_reviews->rating == 2)


                                                <div class="text-primary mr-2">
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                </div>
                                                @elseif ($show_reviews->rating == 3)


                                                <div class="text-primary mr-2">
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                </div>

                                                @elseif ($show_reviews->rating == 4)


                                                <div class="text-primary mr-2">
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="far fa-star"></small>
                                                </div>
                                                @else
                                                <div class="text-primary mr-2">
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                    <small class="fas fa-star"></small>
                                                </div>

                                                @endif
                                            </h6>
                                            <h6> User Review :{{ $show_reviews->review }}</h6>
                                        </div>
                                    </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <h4 class="mb-4">Leave a review</h4>
                                <div class="d-flex my-3">
                                    <h6 class="mb-0 mr-2">Product Rating * :

                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#exampleModalCenter">
                                            Click Here For Product Review
                                        </button>

                                        <!-- Modal -->
                                    <form action="{{ route('product.starrating') }}" method="post"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                                            aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">Product Review
                                                        </h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <input type="hidden" value="{{ $user_detail['id'] }}"
                                                        name="id">

                                                    <div class="modal-body">

                                                        <div class="container">
                                                            <div id="rate"
                                                                class="starrating risingstar d-flex justify-content-center flex-row-reverse">
                                                                <input type="radio" id="star5" name="rating"
                                                                    value="5" /><label for="star5"
                                                                    title="5 star">5</label>
                                                                <input type="radio" id="star4" name="rating"
                                                                    value="4" /><label for="star4"
                                                                    title="4 star">4</label>
                                                                <input type="radio" id="star3" name="rating"
                                                                    value="3" /><label for="star3"
                                                                    title="3 star">3</label>
                                                                <input type="radio" id="star2" name="rating"
                                                                    value="2" /><label for="star2"
                                                                    title="2 star">2</label>
                                                                <input type="radio" id="star1" name="rating"
                                                                    value="1" /><label for="star1"
                                                                    title="1 star">1</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="name">Your Name *</label>
                                                            <input required name="name" type="text"
                                                                class="form-control @error('name') is-invalid @enderror""
                                                                id="name">
                                                            <input type="hidden" value="{{ $user_detail['id'] }}"
                                                                name="id">
                                                            @error('name')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror

                                                        </div>
                                                        <div class="form-group">
                                                            <label for="message">Your Review *</label>
                                                            <textarea required name="review" id="message" cols="30" rows="5"
                                                                class="form-control @error('review') is-invalid @enderror""></textarea>
                                                            @error('review')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                        <form>
                                                            <div class="form-group">
                                                                <label>Product PIcture</label>
                                                                <div class="image">
                                                                    <label><h4>Add image</h4></label>
                                                                    <input type="file" class="form-control" required name="image">
                                                                  </div>
                                                            </div>
                                                        </form>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Save
                                                            changes</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </form>

                                </div>
                                {{-- <form action="{{ route('product.review') }}" method="POST"
                                    enctype="multipart/form-data">
                                    @csrf


                                    <div class="form-group">
                                        <label for="message">Your Review *</label>
                                        <textarea required name="review" id="message" cols="30" rows="5"
                                            class="form-control @error('review') is-invalid @enderror""></textarea>
                                        @error('review')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Your Name *</label>
                                        <input required name="name" type="text"
                                            class="form-control @error('name') is-invalid @enderror"" id="name">
                                        <input type="hidden" value="{{ $user_detail['id'] }}" name="id">
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                    </div>
                                    <div class="form-group mb-0">
                                        <input type="submit"  value="Leave Your Review" class="btn btn-primary px-3">
                                    </div>
                                </form> --}}
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab-pane-2">
                        <h4 class="mb-3">Additional Information</h4>
                        <p>Eos no lorem eirmod diam diam, eos elitr et gubergren diam sea. Consetetur vero aliquyam invidunt
                            duo dolores et duo sit. Vero diam ea vero et dolore rebum, dolor rebum eirmod consetetur
                            invidunt sed sed et, lorem duo et eos elitr, sadipscing kasd ipsum rebum diam. Dolore diam stet
                            rebum sed tempor kasd eirmod. Takimata kasd ipsum accusam sadipscing, eos dolores sit no ut diam
                            consetetur duo justo est, sit sanctus diam tempor aliquyam eirmod nonumy rebum dolor accusam,
                            ipsum kasd eos consetetur at sit rebum, diam kasd invidunt tempor lorem, ipsum lorem elitr
                            sanctus eirmod takimata dolor ea invidunt.</p>
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item px-0">
                                        Sit erat duo lorem duo ea consetetur, et eirmod takimata.
                                    </li>
                                    <li class="list-group-item px-0">
                                        Amet kasd gubergren sit sanctus et lorem eos sadipscing at.
                                    </li>
                                    <li class="list-group-item px-0">
                                        Duo amet accusam eirmod nonumy stet et et stet eirmod.
                                    </li>
                                    <li class="list-group-item px-0">
                                        Takimata ea clita labore amet ipsum erat justo voluptua. Nonumy.
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item px-0">
                                        Sit erat duo lorem duo ea consetetur, et eirmod takimata.
                                    </li>
                                    <li class="list-group-item px-0">
                                        Amet kasd gubergren sit sanctus et lorem eos sadipscing at.
                                    </li>
                                    <li class="list-group-item px-0">
                                        Duo amet accusam eirmod nonumy stet et et stet eirmod.
                                    </li>
                                    <li class="list-group-item px-0">
                                        Takimata ea clita labore amet ipsum erat justo voluptua. Nonumy.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="tab-pane-3">
                        <h4 class="mb-3">Product Description</h4>
                        <p>Eos no lorem eirmod diam diam, eos elitr et gubergren diam sea. Consetetur vero aliquyam invidunt
                            duo dolores et duo sit. Vero diam ea vero et dolore rebum, dolor rebum eirmod consetetur
                            invidunt sed sed et, lorem duo et eos elitr, sadipscing kasd ipsum rebum diam. Dolore diam stet
                            rebum sed tempor kasd eirmod. Takimata kasd ipsum accusam sadipscing, eos dolores sit no ut diam
                            consetetur duo justo est, sit sanctus diam tempor aliquyam eirmod nonumy rebum dolor accusam,
                            ipsum kasd eos consetetur at sit rebum, diam kasd invidunt tempor lorem, ipsum lorem elitr
                            sanctus eirmod takimata dolor ea invidunt.</p>
                        <p>Dolore magna est eirmod sanctus dolor, amet diam et eirmod et ipsum. Amet dolore tempor
                            consetetur sed lorem dolor sit lorem tempor. Gubergren amet amet labore sadipscing clita clita
                            diam clita. Sea amet et sed ipsum lorem elitr et, amet et labore voluptua sit rebum. Ea erat sed
                            et diam takimata sed justo. Magna takimata justo et amet magna et.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Shop Detail End -->


    <!-- Products Start -->
    <div class="container-fluid py-5">
        <div class="text-center mb-4">
            <h2 class="section-title px-5"><span class="px-2">You May Also Like</span></h2>
        </div>
        <div class="row px-xl-5">
            <div class="col">
                <div class="owl-carousel related-carousel">

                    @foreach ($products as $product)
                        <form action="{{ route('cart.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="card product-item border-0">
                                <div
                                    class="card-header product-img position-relative overflow-hidden bg-transparent border p-0">
                                    <img class="img-fluid w-100" src="{{  '/img/'.$product['image'] }}" alt="">
                                </div>
                                <div class="card-body border-left border-right text-center p-0 pt-4 pb-3">
                                    <h6 class="text-truncate mb-3">{{ $product['name'] }}</h6>
                                    <div class="d-flex justify-content-center">
                                        <h6>{{ $product['price'] }}</h6>
                                        <h6 class="text-muted ml-2"><del>$123.00</del></h6>
                                    </div>
                                </div>
                                <input type="hidden" value="{{ $product->id }}" name="id">
                                <input type="hidden" value="{{ $product->name }}" name="name">
                                <input type="hidden" value="{{ $product->price }}" name="price">
                                <input type="hidden" value="1" name="quantity">

                                <div class="card-footer text-center  bg-light border">
                                    <div class="card-footer bg-light border">
                                        <a href="{{ route('shop_detail', ['id' => base64_encode($product->id)]) }}"
                                            class="btn btn-sm text-dark p-0"><i
                                                class="fas fa-eye text-primary mr-1"></i>View Detail</a>
                                        {{-- <a href="" class="btn btn-sm text-dark p-0 "><i
                                                class="fas fa-shopping-cart text-primary mr-1">
                                                <button style="border: none"
                                                    class=" btn btn-sm text-dark text-white bg-blue-800 rounded">Add
                                                    To Cart</button></i>
                                        </a> --}}
                                    </div>
                                </div>
                            </div>
                        </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- Products End -->


    <!-- Footer Start -->

    <!-- Footer End -->


    <!-- Back to Top -->
    <a href="#" class="btn btn-primary back-to-top"><i class="fa fa-angle-double-up"></i></a>
@endsection


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

{{--

<script>
 $(document).on("click", '#rating', function(e) {
        e.preventDefault();

        let name = $('input[name=rating]').val();
        console.log(name);
        rating();
        function rating() {
            $.ajax({
                method: "POST",
                data: {
                    "token": "{{ csrf_token() }}",
                    "rating":name
                },
                url: "/product_starrating",
                success: function(response) {
                    console.log(response);
                    $('.rateview').html(response);

                }
            });
        }
    });


</script> --}}
