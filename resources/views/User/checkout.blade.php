@extends('User.layouts.master')

@section('content')
    <!-- Topbar Start -->

    <!-- Topbar End -->


    <!-- Navbar Start -->

    @if (Session::get('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif
    @if (Session::get('error'))
        <div class="alert alert-danger">
            {{ Session::get('error') }}
        </div>
    @endif


    <!-- Checkout Start -->
    <div class="container-fluid pt-5">

        <div class="row px-xl-5">
            <div class="col-lg-8">
                <div class="mb-4">
                    <h4 class="font-weight-semi-bold mb-4">Billing Address</h4>


                    <div class="row border-right">
                        <form method="post" action="{{ url('user_addresses') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="p-3 py-5">
                                {{-- <input name="_token" type="hidden" value="{{ csrf_token() }}" /> --}}
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <label class="labels">Name</label>
                                        <input type="text" name="name"
                                            class="form-control @error('name') is-invalid @enderror"
                                            value="{{ Auth::user()->name }}">
                                        @error('name')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label class="labels">Mobile Number</label>
                                        <input type="text" name="mobile_number"
                                            class="form-control @error('mobile_number') is-invalid @enderror"
                                            value="{{ Auth::user()->mobile_number }}">
                                        @error('mobile_number')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6"><label class="labels">Street</label><input type="text"
                                            name="street" class="form-control @error('street') is-invalid @enderror"
                                            value="{{ old('street') }}">
                                        @error('street')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6"><label class="labels">Lankmark</label><input type="text"
                                            name="landmark" class="form-control @error('landmark') is-invalid @enderror"
                                            value="{{ old('landmark') }}">
                                        @error('landmark')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6"><label class="labels">Address</label><input type="text"
                                            name="address" class="form-control @error('address') is-invalid @enderror"
                                            value="{{ Auth::user()->address }}">
                                        @error('address')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6"><label class="labels">Postcode</label><input type="text"
                                            name="postcode" class="form-control @error('postcode') is-invalid @enderror"
                                            value="{{ Auth::user()->postcode }}">
                                        @error('postcode')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6"><label class="labels">State</label><input type="text"
                                            name="state" class="form-control @error('state') is-invalid @enderror"
                                            value="{{ Auth::user()->state }} ">
                                        @error('state')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6"><label class="labels">Email ID</label><input type="text"
                                            name="email" class="form-control @error('email') is-invalid @enderror"
                                            value="{{ Auth::user()->email }}">
                                        @error('email')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-6"><label class="labels">Country</label><input type="text"
                                            name="country" class="form-control @error('country') is-invalid @enderror"
                                            value="{{ Auth::user()->country }}">
                                        @error('country')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="mt-5 text-center">
                                    <button class="btn btn-primary profile-button" style="color: white" type="submit">
                                        save
                                    </button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <div class="col-lg-4">
                <div class="card border-secondary mb-5">
                    <div class="card-header bg-secondary border-0">
                        <h4 class="font-weight-semi-bold m-0">Order Total</h4>
                    </div>
                    <div class="card-body ">
                        <h5 class="font-weight-medium mb-3">Products</h5>
                        <table class="table" id="cart-products">
                            <tr>
                                <th scope="col" name="product_name">Name</th>
                                <th scope="col" name="product_name" style="float: right">Quantity</th>
                                {{-- <th scope="col" name="product_price" style="float: right">Price</th> --}}
                            </tr>
                        </table>

                        <hr class="mt-0">
                        <div class="d-flex justify-content-between mb-3 pt-1">
                            <h6 class="font-weight-medium">Subtotal</h6>
                            <h6 class="font-weight-medium cart-total">$150</h6>
                        </div>
                        <div class="d-flex justify-content-between">
                            <h6 class="font-weight-medium">Shipping</h6>
                            <h6 class="font-weight-medium">$10</h6>
                        </div>
                    </div>
                    <div class="card-footer border-secondary bg-transparent">
                        <div class="d-flex justify-content-between mt-2">
                            <h5 class="font-weight-bold">Total</h5>
                            <h5 class="font-weight-bold cart-price"></h5>
                        </div>
                    </div>
                </div>

                <div class="col-12 mt-4">
                    <div class="card p-3">
                        <p class="mb-0 fw-bold h4">Payment</p>
                        <div id="app">
                            <main class="py-4">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-6 offset-3 col-md-offset-6">

                                            <div class="card card-default">
                                                <div class="card-body text-center">
                                                    <form action="{{ route('razorpay.payment.store') }}" method="POST">
                                                        @csrf
                                                        <script src="https://checkout.razorpay.com/v1/checkout.js" data-key="{{ env('RAZORPAY_KEY') }}" data-amount="{{$cart_total}}"
                                                            data-buttontext="Pay" data-name="ProMat" data-description="Rozerpay"
                                                            data-image="https://www.itsolutionstuff.com/frontTheme/images/logo.png" data-prefill.name="name"
                                                            data-prefill.email="email" data-theme.color="#ff7529"></script>
                                                    </form>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </main>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
    <!-- Checkout End -->


    <!-- Footer Start -->

    <a href="#" class="btn btn-primary back-to-top"><i class="fa fa-angle-double-up"></i></a>
@endsection


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script>
    $(document).ready(function() {
        loadcart();

        function loadcart() {
            $.ajax({
                method: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": 10,
                },
                url: "/cart_total",
                success: function(response) {
                    console.log('cart total are = ', response);
                    $('.cart-total').html(response['cart_total']);
                    $('.cart-price').html(response['price']);

                    $.each(response['cart_products'], function(index, obj) {
                        console.log(index);
                        console.log(obj);
                        var tr = $("<tr ></tr>");

                        tr.append("<td >" + obj.name + "</td>");
                        tr.append("<td style='float: right'>" + obj.quantity + "</td>");
                        $("#cart-products").append(tr);
                    });
                }
            });
        }
    });





    $(document).on("click", '#order', function(e) {
        e.preventDefault();

        order_detail();

        function order_detail() {
            $.ajax({
                method: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                url: "/order_placed",
                success: function(response) {
                    console.log('done');
                    document.location =
                        './order_placed_view'; // makes the browser to navigate to a new url



                }
            });
        }
    });
</script>
