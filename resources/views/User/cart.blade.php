@extends('User.layouts.master')

@section('content')
    <!-- Topbar Start -->

    <!-- Topbar End -->


    <!-- Navbar Start -->




    <!-- Cart Start -->
    <div class="container-fluid pt-5">
        <div class="row px-xl-5">
            <div class="col-lg-8 table-responsive mb-5">

                <main class="my-8">
                    <div class="container px-6 mx-auto">
                        <div class="flex justify-center my-6">
                            <div
                                class=" container-fluid flex flex-col w-full p-8 text-gray-800 bg-white shadow-lg pin-r pin-y md:w-4/5 lg:w-4/5">
                                @if ($message = Session::get('success'))
                                    <div class="p-4 mb-3 bg-green-400 rounded">
                                        <p class="text-green-800">{{ $message }}</p>
                                    </div>
                                @endif
                                <h3 class="text-3xl py-3 text-bold">Cart List</h3>
                                <div class="flex-1">
                                    <table class="w-full text-sm lg:text-base table" cellspacing="0">
                                        <thead>
                                            <tr class="h-12 uppercase">
                                                <th scope="col" class="hidden md:table-cell"></th>
                                                <th class="text-left">Name</th>
                                                <th class="hidden text-right md:table-cell">Item price</th>
                                                <th class="pl-5 text-left lg:text-right lg:pl-0">
                                                    <span class="lg:hidden" title="Quantity">Qtd</span>
                                                    <span class="hidden lg:inline">Quantity</span>
                                                </th>
                                                <th>Item Total</th>
                                                <th>size</th>
                                                <th class="hidden text-right md:table-cell"> Remove </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($cartItems as $item)
                                                <tr>
                                                    <td class="hidden pb-4 md:table-cell">
                                                        <a href="#">
                                                            {{-- <img src="{{ $item->attributes->image }}" class="w-20 rounded" alt="Thumbnail"> --}}
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('shop_detail', ['id' => base64_encode($item->product_id)]) }}">
                                                            <p class="mb-2 md:ml-4">{{ $item->name }}</p>

                                                        </a>
                                                    </td>
                                                    <td class="hidden text-center md:table-cell">
                                                        <span class="text-sm font-medium lg:text-base">
                                                            ${{ $item->price }}
                                                        </span>
                                                    </td>
                                                    <td class="justify-center mt-6 md:justify-end md:flex">
                                                        <div class="h-10 w-28">
                                                            <div class="relative flex flex-row w-full h-8">

                                                                <form action="{{ route('cart.update') }}" method="POST">
                                                                    @csrf
                                                                    <input type="hidden" name="id"
                                                                        value="{{ $item->id }}">
                                                                    <input type="number" name="quantity"
                                                                        value="{{ $item->quantity }}"
                                                                        class="w-6 text-center bg-gray-300" />
                                                                    <button type="submit"
                                                                        style="border: none; background-color:#D19C97; color:black;"
                                                                        class="px-2 pb-2 ml-2 text-white bg-blue-500">update</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="hidden text-center md:table-cell">
                                                        <span class="text-sm font-medium lg:text-base">
                                                            ${{ $item->total }}
                                                        </span>
                                                    </td>
                                                    <td class="hidden text-left md:table-cell">
                                                        <span class="text-sm font-medium lg:text-base">
                                                            {{ $item->size }}
                                                        </span>
                                                    </td>
                                                    <td class="hidden text-right md:table-cell">
                                                        <form action="{{ route('cart.remove') }}" method="POST">
                                                            @csrf
                                                            <input type="hidden" value="{{ $item->id }}"
                                                                name="id">
                                                            <button class="px-4 py-2 text-white bg-red-600"
                                                                style="border: none; background-color:#D19C97;">x</button>
                                                        </form>

                                                    </td>
                                                </tr>
                                            @endforeach

                                        </tbody>
                                    </table>
                                    Total Amount
                                    <div class="cart-total">

                                    </div>
                                    <div class="py-3">
                                        <form action="{{ route('cart.clear') }}" method="POST">
                                            @csrf
                                            <button class="px-6 py-2 text-red-800 bg-red-300">Remove All Cart</button>
                                        </form>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div class="col-lg-4">
                <form class="mb-5" action="">
                    <div class="input-group">
                        <input type="text" class="form-control p-4" placeholder="Coupon Code">
                        <div class="input-group-append">
                            <button class="btn btn-primary">Apply Coupon</button>
                        </div>
                    </div>
                </form>
                <div class="card border-secondary mb-5">
                    <div class="card-header bg-secondary border-0">
                        <h4 class="font-weight-semi-bold m-0">Cart Summary</h4>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between mb-3 pt-1">
                            <h6 class="font-weight-medium">Subtotal</h6>
                            <h6 class="font-weight-medium">
                                <div class="cart-total">
                                </div>

                            </h6>
                        </div>
                        <div class="d-flex justify-content-between">
                            <h6 class="font-weight-medium">Shipping</h6>
                            <h6 class="font-weight-medium">$10</h6>
                        </div>
                    </div>
                    <div class="card-footer border-secondary bg-transparent">
                        <div class="d-flex justify-content-between mt-2">
                            <h5 class="font-weight-bold">Total</h5>
                            <h5 class="font-weight-bold">
                                <div class="cart-price">

                                </div>
                            </h5>
                        </div>
                        <a href="checkout"> <button class="btn btn-block btn-primary my-3 py-3">Proceed To
                                Checkout</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Cart End -->


    <!-- Footer Start -->

    <!-- Footer End -->


    <!-- Back to Top -->
    <a href="#" class="btn btn-primary back-to-top"><i class="fa fa-angle-double-up"></i></a>
@endsection


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script>
    $(document).ready(function() {
        loadcart();

        function loadcart() {
            $.ajax({
                method: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": 10,
                },
                url: "/cart_total",
                success: function(response) {
                    console.log('cart total are = ', response);
                    $('.cart-total').html(response['cart_total']);
                    $('.cart-price').html(response['price']);
                }
            });
        }
    });
</script>
