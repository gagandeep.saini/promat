@extends('User.layouts.master')

@section('content')
    <!-- Topbar Start -->

    <!-- Topbar End -->


    <!-- Navbar Start -->

    <!-- Shop Start -->
    @if (Session::get('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif
    @if (Session::get('error'))
        <div class="alert alert-danger">
            {{ Session::get('error') }}
        </div>
    @endif

    <div class="container-fluid  align-items-center justify-content-between">
        <div class="d-flex align-items-center justify-content-center mb-4">
            <form action="{{ route('cart.search') }}" method="Get" enctype="multipart/form-data">
                @csrf
                <div class="input-group">
                    <input type="text" name="query" class="form-control" placeholder="Search by name">
                    <div class="input-group-append">
                        <span class="input-group-text bg-transparent text-primary">
                            <button class="" type="submit" style="border: none">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </div>


            </form>
        </div>


        <div class="container-fluid d-flex align-items-center justify-content-center">
            <div class="row d-flex justify-content-center ">
                @foreach ($products as $product)
                    <div class="col-3 rounded-md">
                        <div class="card-header product-img position-relative overflow-hidden bg-transparent border p-3">
                            <img src="{{ url( '/img/'.$product->image) }}" alt="" class="">
                        </div>

                        <div class="">
                            <h3 class="text-gray-700 uppercase">{{ $product->name }}</h3>
                            <span class="mt-2 text-gray-500">${{ $product->price }}</span>
                            <form action="{{ route('cart.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" value="{{ $product->id }}" name="id">
                                <input type="hidden" value="{{ $product->name }}" name="name">
                                <input type="hidden" value="{{ $product->price }}" name="price">
                                {{-- <input type="hidden" value="{{ $product->image }}"  name="image" height="100px" width="100px"> --}}
                                <input type="hidden" value="1" name="quantity">
                                <div class="card-footer text-center  bg-light border">
                                    <div class="card-footer bg-light border">
                                        <a href="{{ route('shop_detail', ['id' => base64_encode($product->id)]) }}"
                                            class="btn btn-sm text-dark p-0"><i
                                                class="fas fa-eye text-primary mr-1"></i>View Detail</a>
                                        {{-- <a href="" class="btn btn-sm text-dark p-0 "><i
                                                    class="fas fa-shopping-cart text-primary mr-1">
                                                    <button style="border: none"
                                                        class=" btn btn-sm text-dark text-white bg-blue-800 rounded">Add
                                                        To Cart</button></i>
                                            </a> --}}
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>


        {{-- <span class="paginate">
                {{ $products->links() }}

            </span> --}}
        <style>
            .w-5 {
                display: none;
            }

            .paginate {
                display: flex;
                align-content: center;
                justify-content: center;
                align-items: center
            }
        </style>
        <!-- Shop Product End -->
    </div>

    <a href="#" class="btn btn-primary back-to-top"><i class="fa fa-angle-double-up"></i></a>
@endsection
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>
