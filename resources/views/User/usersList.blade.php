@extends('User.layouts.master')


@section('content')
    <div class="container mt-5">
        <table class="table table-inverse">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($user as $data)
                    <tr id="student{{ $data->id }}">
                        <td>{{ $data->name }}</td>
                        <td>{{ $data->email }}</td>
                        <td><img src="/storage/images/{{ $data->image }}" height="50px" width="50px" alt=""></td>
                        <td>
                            <a href="{{ url('/all_users/edit', $data->id) }}">
                                <i class="fa fa-pen">
                                </i>
                            </a>
                            <a href="{{ url('/all_users/delete', $data->id) }}">
                                <i class="fa fa-trash">
                                </i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
