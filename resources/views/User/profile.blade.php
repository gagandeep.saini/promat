@extends('User.layouts.master')


@section('content')

    <div class="container-fluid">
        @if (Session::get('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        @endif
        @if (Session::get('error'))
            <div class="alert alert-danger">
                {{ Session::get('error') }}
            </div>
        @endif
        <div class="container rounded bg-white mt-5 mb-5">
            <div class="row">
                <div class="col-md-3 border-right">
                    <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                         @if(Auth::user()->role == 1)
                            <h5>Admin</h5>
                         @else
                            <h5>User</h5>

                         @endif
                        @if (Auth::user()->image)
                            <img class="rounded-circle mt-5" width="200px" height="200px"
                                src="{{ asset(Auth::user()->image) }}" alt="profile_image">
                        @endif
                        @if (Auth::User()->image == null)
                            <img class="rounded-circle mt-5" width="200px"
                                src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
                        @endif
                        <span class="font-weight-bold">{{ Auth::user()->name }}</span><span
                            class="text-black-50">{{ Auth::user()->email }}</span><span> </span>
                    </div>
                    <form action="{{ route('upload_profile_picture') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div>

                            <input type="file" name="image" class="change_profile">
                        </div>

                        <div class="mt-5 text-center">
                            <button class="btn btn-primary profile-button" type="submit">
                                <input type="submit" style="background-color: transparent; border:none; color:white"
                                    value="Upload"> </button>
                        </div>
                    </form>
                </div>
                <div class="col-md-5 border-right">
                    <form method="post" action="{{ url('profile') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="p-3 py-5">
                            <div class="d-flex justify-content-between align-items-center mb-3">
                                <h4 class="text-right">Profile Settings</h4>
                            </div>
                            {{-- <input name="_token" type="hidden" value="{{ csrf_token() }}" /> --}}
                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <label class="labels">Name</label>
                                    <input type="text" name="name"
                                        class="form-control @error('name') is-invalid @enderror"
                                        value="{{ Auth::user()->name }}">
                                    @error('name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-12">
                                    <label class="labels">Mobile Number</label>
                                    <input type="text" name="mobile_number"
                                        class="form-control @error('mobile_number') is-invalid @enderror"
                                        value="{{ Auth::user()->mobile_number }}">
                                    @error('mobile_number')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-12"><label class="labels">Address</label><input type="text"
                                        name="address" class="form-control @error('address') is-invalid @enderror"
                                        value="{{ Auth::user()->address }}">
                                    @error('address')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-12"><label class="labels">Postcode</label><input type="text"
                                        name="postcode" class="form-control @error('postcode') is-invalid @enderror"
                                        value="{{ Auth::user()->postcode }}">
                                    @error('postcode')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-12"><label class="labels">State</label><input type="text"
                                        name="state" class="form-control @error('state') is-invalid @enderror"
                                        value="{{ Auth::user()->state }} ">
                                    @error('state')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-12"><label class="labels">Email ID</label><input type="text"
                                        name="email" class="form-control @error('email') is-invalid @enderror"
                                        value="{{ Auth::user()->email }}">
                                    @error('email')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-12"><label class="labels">Country</label><input type="text"
                                        name="country" class="form-control @error('country') is-invalid @enderror"
                                        value="{{ Auth::user()->country }}">
                                    @error('country')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="mt-5 text-center">
                                <button class="btn btn-primary profile-button" style="color: white" type="submit">
                                    save
                                </button>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="col-md-4">
                    <div class="p-3 py-5">
                        <div class="d-flex justify-content-between align-items-center experience"><span>Change Password
                            </span></div><br>
                            <form action="{{ route('change_password') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                        <div class="col-md-12"><label class="labels">Current Password</label>
                            <input type="text"
                                class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Current Password" value="">
                                @error('password')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror</div>
                        <div class="col-md-12"><label class="labels">New Password</label>
                            <input type="text"
                                class="form-control @error('new_password') is-invalid @enderror"  name="new_password" placeholder="New Password" value="">
                                @error('new_password')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror</div>
                        <div class="col-md-12"><label class="labels">Confirm Password</label>
                            <input type="text"
                                class="form-control @error('confirm_password') is-invalid @enderror"  name="confirm_password" placeholder="Confirm Password" value="">
                                @error('confirm_password')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror</div>
                                <div class="mt-5 text-center">
                                    <button class="btn btn-primary profile-button" style="color: white" type="submit">
                                        Update Password
                                    </button>
                                </div>
                                </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@endsection
