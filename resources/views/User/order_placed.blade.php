@extends('User.layouts.master')

@section('content')
<div class="card text-center">
    @if (Session::get('success'))
    <div class="alert alert-success">
        {{ Session::get('success') }}
    </div>
@endif
@if (Session::get('error'))
    <div class="alert alert-danger">
        {{ Session::get('error') }}
    </div>
@endif
    <div class="card-header">
      Order Status
    </div>
    <div class="card-body">
      <h1 class="card-title">Order Placed Successfully</h1>
     <b> <p class="card-text">Thanks for shopping , order placed successfully
        .</p></b> <br>
      <a href="home" class="btn btn-primary">Go back to home page</a>
    </div>
    <div class="card-footer text-muted">
    </div>
  </div>
@endsection
