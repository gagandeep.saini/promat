<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a href="" class="text-decoration-none">
            <h1 class="m-0 display-5 font-weight-semi-bold"><span
                    class="text-primary font-weight-bold border px-3 mr-1">P</span>roMat</h1>
        </a>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="home" class="nav-item nav-link">Home</a>
                    </i>
                <li class="nav-item">
                    <a href="shop" class="nav-item nav-link">Shop</a>
                    </i>


                <li class="nav-item">
                    <a href="contact" class="nav-item nav-link">Contact</a>
                    </i>
                </li>
            </ul>
        </div>
        <div class="navbar-nav ml-auto py-0">
            <div class=" text-right">
                <a href="cart">
                    <i class="fas fa-shopping-cart text-primary"></i>
                    <span class="badge" style="color: black" id="cart-count">056</span>
                </a>
            </div>
        </div>

        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown"
                aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                    {{ Auth::user()->name }}
                </span>
            </a>

            <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <li><a class="dropdown-item" href="profile">Profile</a></li>
                <li><a class="dropdown-item" href="#" data-toggle="modal" style="text-decoration: none"
                        data-target="#logoutModal">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>


<script src="https://www.markuptag.com/bootstrap/5/js/bootstrap.bundle.min.js"></script>



<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script>
    $(document).ready(function() {
        loadcart();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function loadcart() {
            $.ajax({
                method: "GET",
                url: "/cart_item",
                success: function(response) {
                    console.log('cart items are = ', response);
                    $('#cart-count').html('');
                    $('#cart-count').html(response);
                }
            });
        }
    });
</script>
