<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->bigInteger('mobile_number');
            $table->string('street');
            $table->string('landmark');
            $table->string('address');
            $table->bigInteger('postcode');
            $table->string('state');
            $table->string('email');
            $table->string('country');
            // $table->string('product_name');
            // $table->string('product_price');
            $table->bigInteger('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
