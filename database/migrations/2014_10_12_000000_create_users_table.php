<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->bigInteger('mobile_number')->default(0);
            $table->bigInteger('postcode');
            $table->integer('role')->default(2);
            $table->string('email')->unique();
            $table->string('address');
            $table->string('state');
            $table->string('country');
            $table->string('password');
            $table->string('token');
            $table->string('image')->default('/public/Image/202210070625Screenshot%20from%202022-09-07%2016-43-06.png');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
