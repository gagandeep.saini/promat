<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserQuerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_queries')->insert([
            'name' => 'Gagan',
            'email' => 'gagandeep39393@gmail.com',
            'subject' => 'need a help',
            'message' => 'need a help',
        ]);
        DB::table('user_queries')->insert([
            'name' => 'Gagan',
            'email' => 'gagandeep393939@gmail.com',
            'subject' => 'need a help',
            'message' => 'need a help',
        ]);
        DB::table('user_queries')->insert([
            'name' => 'Gagan',
            'email' => 'gagandeep39339@gmail.com',
            'subject' => 'need a help',
            'message' => 'need a help',
        ]);
    }
}
