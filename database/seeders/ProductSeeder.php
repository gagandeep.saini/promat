<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => 'Girl Shirts',
            'price' => 200,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-5.jpg'
        ]);
        Product::create([
            'name' => 'Ladies Shirts',
            'price' => 200,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-1.jpg'
        ]);
        Product::create([
            'name' => 'Boy Shirts',
            'price' => 200,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-2.jpg'
        ]);
        Product::create([
            'name' => 'Gents Shirts',
            'price' => 200,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-6.jpg'
        ]);
        Product::create([
            'name' => 'Girl Jeans',
            'price' => 300,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-5.jpg'
        ]);
        Product::create([
            'name' => 'Ladies Jeans',
            'price' => 300,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-1.jpg'
        ]);
        Product::create([
            'name' => 'Boy Jeans',
            'price' => 300,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-2.jpg'
        ]);
        Product::create([
            'name' => 'Gents Jeans',
            'price' => 300,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-6.jpg'
        ]);
        Product::create([
            'name' => 'Girl Swimwear',
            'price' => 400,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-5.jpg'
        ]);
        Product::create([
            'name' => 'Ladies Swimwear',
            'price' => 400,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-1.jpg'
        ]);
        Product::create([
            'name' => 'Boy Swimwear',
            'price' => 400,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-2.jpg'
        ]);
        Product::create([
            'name' => 'Gents Swimwear',
            'price' => 400,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-6.jpg'
        ]);
        Product::create([
            'name' => 'Girl Sleepwear',
            'price' => 200,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-5.jpg'
        ]);
        Product::create([
            'name' => 'Ladies Sleepwear',
            'price' => 200,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-1.jpg'
        ]);
        Product::create([
            'name' => 'Boy Sleepwear',
            'price' => 200,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-2.jpg'
        ]);
        Product::create([
            'name' => 'Gents Sleepwear',
            'price' => 200,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-6.jpg'
        ]);
        Product::create([
            'name' => 'Girl Sportswear',
            'price' => 300,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-5.jpg'
        ]);
        Product::create([
            'name' => 'Ladies Sportswear',
            'price' => 300,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-1.jpg'
        ]);
        Product::create([
            'name' => 'Boy Sportswear',
            'price' => 300,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-2.jpg'
        ]);
        Product::create([
            'name' => 'Gents Sportswear',
            'price' => 300,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-6.jpg'
        ]);
        Product::create([
            'name' => 'Girl Jumpsuits',
            'price' => 400,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-5.jpg'
        ]);
        Product::create([
            'name' => 'Ladies Jumpsuits',
            'price' => 400,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-1.jpg'
        ]);
        Product::create([
            'name' => 'Boy Jumpsuits',
            'price' => 400,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-2.jpg'
        ]);
        Product::create([
            'name' => 'Gents Jumpsuits',
            'price' => 400,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-6.jpg'
        ]);
        Product::create([
            'name' => 'Girl Blazers',
            'price' => 200,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-5.jpg'
        ]);
        Product::create([
            'name' => 'Ladies Blazers',
            'price' => 200,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-1.jpg'
        ]);
        Product::create([
            'name' => 'Boy Blazers',
            'price' => 200,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-2.jpg'
        ]);
        Product::create([
            'name' => 'Gents Blazers',
            'price' => 200,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-6.jpg'
        ]);
        Product::create([
            'name' => 'Girl Jackets',
            'price' => 300,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-5.jpg'
        ]);
        Product::create([
            'name' => 'Ladies Jackets',
            'price' => 300,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-1.jpg'
        ]);
        Product::create([
            'name' => 'Boy Jackets',
            'price' => 300,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-2.jpg'
        ]);
        Product::create([
            'name' => 'Gents Jackets',
            'price' => 300,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-6.jpg'
        ]);
        Product::create([
            'name' => 'Girl Shoes',
            'price' => 400,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-5.jpg'
        ]);
        Product::create([
            'name' => 'Ladies Shoes',
            'price' => 400,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-1.jpg'
        ]);
        Product::create([
            'name' => 'Boy Shoes',
            'price' => 400,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-2.jpg'
        ]);
        Product::create([
            'name' => 'Gents Shoes',
            'price' => 400,
            'description' => 'A watch is a portable timepiece intended to be carried or worn by a person. It is designed to keep a consistent movement despite the motions caused by the person',
            'image' => 'product-6.jpg'
        ]);
    }
}
