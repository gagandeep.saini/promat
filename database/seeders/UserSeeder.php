<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Gagan',
            'role' => '1',
            'mobile_number' => '7626835373',
            'address' => 'Hoshiarpur Punjab',
            'postcode' => '146113',
            'state' => 'Punjab',
            'country' => 'India',
            'email' => 'gagandeep393939@gmail.com',
            'password' => Hash::make('13@Deep13'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'token'=>"ghgjgjgk",
            'image'=>"product-1.jpg"
        ]);
        DB::table('users')->insert([
            'name' => 'Deep',
            'role' => '2',
            'mobile_number' => '7626835373',
            'address' => 'Hoshiarpur Punjab',
            'postcode' => '146113',
            'state' => 'Punjab',
            'country' => 'India',
            'email' => 'gagansainisaini711@gmail.com',
            'password' => Hash::make('13@Deep13'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'token'=>"ghgjgjgk",
            'image'=>"product-2.jpg"

        ]);
    }
}
