<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
// use Illuminate\Routing\Route;

use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\UserControllers\CartController;
use App\Http\Controllers\UserControllers\HomeController;
use App\Http\Controllers\UserControllers\RazorpayPaymentController;
use App\Http\Controllers\AdminControllers\SessionsController;
use App\Http\Controllers\AdminControllers\AdminHomeController;
use App\Http\Controllers\AdminControllers\ChangePasswordController;
use App\Http\Controllers\AdminControllers\InfoUserController;
use App\Http\Controllers\AdminControllers\RegisterController;
use App\Http\Controllers\AdminControllers\ResetController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/user', function () {
    // if(auth()->user())
    // {
    //     auth()->user()->assignRole('admin');
    // }
    return view('auth/login');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('sendbasicemail', [ForgotPasswordController::class, 'basic_email']);
Route::get('sendhtmlemail', [ForgotPasswordController::class, 'html_email']);
Route::get('sendattachmentemail', [ForgotPasswordController::class, 'attachment_email']);

Route::group(['middleware' => 'auth','isUser','web'], function () {
    Route::get('profile', [HomeController::class, 'profile'])->name('profile');
    Route::post('profile', [HomeController::class, 'profileUpdate'])->name('profile_update');
    Route::post('upload_profile_picture', [HomeController::class, 'uploadPicture'])->name('upload_profile_picture');
    Route::post('change_password', [HomeController::class, 'changePassword'])->name('change_password');
    Route::post('user_change_password', [HomeController::class, 'userChangePassword'])->name('user_change_password');
    // Route::post('order_placed', [HomeController::class, 'order_placed'])->name('order.placed');
    Route::get('order_placed', [HomeController::class, 'order_placed_view'])->name('order.placed');
    Route::get('shop_detail', [HomeController::class, 'shop_detail'])->name('shop_detail');
    Route::post('user_addresses', [HomeController::class, 'user_addresses'])->name('user_addresses');
    Route::get('users_queries', [HomeController::class, 'users_queries_view'])->name('users_queries_view');
    Route::post('users_queries', [HomeController::class, 'users_queries'])->name('users_queries');
    Route::get('checkout', [HomeController::class, 'checkout'])->name('checkout');
    Route::post('checkout_detail', [HomeController::class, 'checkout_detail'])->name('checkout_detail');
    Route::get('contact', [HomeController::class, 'contact'])->name('contact');
    Route::get('all_users', [HomeController::class, 'all_users'])->name('all_users');
    Route::get('/all_users/delete/{id}', [HomeController::class, 'delete_user'])->name('delete_user');
    Route::get('/all_users/edit/{id}', [HomeController::class, 'edit_user_view'])->name('edit_user_view');
    Route::post('/all_users/edit/', [HomeController::class, 'edit_user'])->name('edit_user');



    Route::get('shop', [CartController::class, 'productList'])->name('products.list');
    Route::get('cart', [CartController::class, 'cartList'])->name('cart.list');
    Route::get('cart_item', [CartController::class, 'cart_item'])->name('cart_item');
    Route::post('cart_total', [CartController::class, 'cart_total'])->name('cart_total');
    Route::post('cart', [CartController::class, 'addToCart'])->name('cart.store');
    Route::post('update-cart', [CartController::class, 'updateCart'])->name('cart.update');
    Route::post('remove', [CartController::class, 'removeCart'])->name('cart.remove');
    Route::post('clear', [CartController::class, 'clearAllCart'])->name('cart.clear');
    Route::get('search', [CartController::class, 'search'])->name('cart.search');
    Route::post('product_starrating', [CartController::class, 'product_starrating'])->name('product.starrating');


    Route::get('razorpay-payment', [RazorpayPaymentController::class, 'index']);
    Route::post('razorpay-payment', [RazorpayPaymentController::class, 'store'])->name('razorpay.payment.store');
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');





















Route::group(['middleware' => 'auth','isAdmin'], function () {

    Route::get('/', [AdminHomeController::class, 'home']);
	// Route::get('dashboard', function () {
	// 	return view('Admin/dashboard');
	// })->name('dashboard');

	Route::get('billing', function () {
		return view('Admin/billing');
	})->name('billing');

	// Route::get('profile', function () {
	// 	return view('Admin/profile');
	// })->name('profile');

	Route::get('rtl', function () {
		return view('Admin/rtl');
	})->name('rtl');

	Route::get('tables', function () {
		return view('Admin/tables');
	})->name('tables');

    Route::get('virtual-reality', function () {
		return view('Admin/virtual-reality');
	})->name('virtual-reality');

    Route::get('static-sign-in', function () {
		return view('Admin/static-sign-in');
	})->name('sign-in');

    Route::get('static-sign-up', function () {
		return view('Admin/static-sign-up');
	})->name('sign-up');


    Route::get('dashboard', [InfoUserController::class, 'dashboard'])->name('dashboard');
    Route::get('orders', [InfoUserController::class, 'orders'])->name('orders');
    Route::get('products', [InfoUserController::class, 'products'])->name('products');
    Route::get('products/edit', [InfoUserController::class, 'products_edit'])->name('products.edit');
    Route::post('products/edit', [InfoUserController::class, 'edit_products']);
    Route::get('products/add', [InfoUserController::class, 'new_product']);
    Route::post('products/add', [InfoUserController::class, 'add_product']);
    Route::get('products/delete/{id}', [InfoUserController::class, 'products_delete'])->name('products_delete');
    Route::get('user-management', [InfoUserController::class, 'all_users']);
    Route::get('user-management/edit', [InfoUserController::class, 'edit'])->name('user.edit');
    Route::post('user-management/edit', [InfoUserController::class, 'edit_user']);
    Route::get('user-management/delete/{id}', [InfoUserController::class, 'delete']);
    Route::get('search-product', [InfoUserController::class, 'search_Product'])->name('product.search');



    Route::get('/logout', [SessionsController::class, 'destroy']);
	Route::get('/user-profile', [InfoUserController::class, 'create']);
	Route::post('/user-profile', [InfoUserController::class, 'store']);
    Route::get('/loginAdmin', function () {
		return view('Admin/dashboard');
	})->name('loginAdmin');
});




Route::group(['middleware' => 'guest'], function () {
    Route::get('/registerAdmin', [RegisterController::class, 'create']);
    Route::post('/registerAdmin', [RegisterController::class, 'store']);
    Route::get('/loginAdmin', [SessionsController::class, 'create']);
    Route::post('/session', [SessionsController::class, 'store']);
	Route::get('/login/forgot-password', [ResetController::class, 'create']);
	Route::post('/forgot-password', [ResetController::class, 'sendEmail']);
	Route::get('/reset-password/{token}', [ResetController::class, 'resetPass'])->name('password.reset');
	Route::post('/reset-password', [ChangePasswordController::class, 'changePassword'])->name('password.update');

});

Route::get('/loginAdmin', function () {
    return view('Admin/session/login-session');
})->name('loginAdmin');
